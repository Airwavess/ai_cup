from NewsCrawler import NewsCrawler

class LibertyTimesCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('LibertyTimes title: ', )
        return html.find(class_='articlebody').find('h1').text.strip()

    def get_article(self, html):
        print('LibertyTimes article: ', )
        return html.find(class_='articlebody').find(class_='text').text