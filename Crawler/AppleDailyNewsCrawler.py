from NewsCrawler import NewsCrawler

class AppleDailyNewsCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('AppleDailyNews title: ',)
        return html.find(class_='ndArticle_leftColumn').find('h1').text

    def get_article(self, html):
        print('AppleDailyNews article: ',)
        article = ''
        for paragraph in html.find(class_='ndArticle_margin').find_all('p'):
            article += paragraph.text
        return article
        