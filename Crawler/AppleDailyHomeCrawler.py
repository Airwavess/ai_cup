from NewsCrawler import NewsCrawler

class AppleDailyHomeCrawler(NewsCrawler):
    def __init__(self):
        pass
    
    def get_title(self, html):
        print('AppleDailyHome title: ', )
        return html.find(class_='ncbox_cont').find('h1').text
    
    def get_article(self, html):
        print('AppleDailyHome article: ', )
        return html.find(class_='articulum').text

