from NewsCrawler import NewsCrawler

class UDNCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('UDN title: ',)
        return html.find(class_='story_art_title').text

    def get_article(self, html):
        print('UDN article: ',)
        article = ''
        for paragraph in html.find(id='story_body_content').find_all('p'):
            article += paragraph.text
        return article