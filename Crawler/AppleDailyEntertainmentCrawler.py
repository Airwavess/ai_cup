from NewsCrawler import NewsCrawler

class AppleDailyEntertainmentCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('AppleDailyEntertainment title: ', )
        return html.find(class_='ndArticle_leftColumn').find('h1').text

    def get_article(self, html):
        print('AppleDailyEntertainment article: ', )
        article = ''
        for paragraph in html.find(class_='ndArticle_content').find_all('p'):
            article += paragraph.text
        return article
