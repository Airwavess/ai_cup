from NewsCrawler import NewsCrawler

class AppleDailyLifestyleCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('AppleDailyLifestyle title: ')
        return html.find(class_='ndArticle_leftColumn').find('h1').text

    def get_article(self, html):
        print('AppleDailyLifestyle article: ')
        article = ''
        for paragraph in html.find(class_='ndArticle_content').find_all(['p', 'h2']):
            article += paragraph.text
        return article