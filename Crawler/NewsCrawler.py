from abc import ABCMeta, abstractmethod

class NewsCrawler(metaclass=ABCMeta):

    @abstractmethod
    def get_title(self, html):
        pass
    
    @abstractmethod
    def get_article(self, html):
        pass
