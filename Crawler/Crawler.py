from AppleDailyCrawler import AppleDailyCrawler
from AppleDailyEntertainmentCrawler import AppleDailyEntertainmentCrawler
from AppleDailyFinanceCrawler import AppleDailyFinanceCrawler
from AppleDailyHomeCrawler import AppleDailyHomeCrawler
from AppleDailyLifestyleCrawler import AppleDailyLifestyleCrawler
from AppleDailyNewsCrawler import AppleDailyNewsCrawler
from AppleDailySportsCrawler import AppleDailySportsCrawler
from ChinaTimesCrawler import ChinaTimesCrawler
from LibertyTimesCrawler import LibertyTimesCrawler
from TBVSNewsCrawler import TBVSNewsCrawler
from UDNCrawler import UDNCrawler

def get_crawler(news_source):
    if news_source == 'http://home.appledaily.com.tw/':
        return AppleDailyHomeCrawler()
    elif news_source == 'http://news.ltn.com.tw/':
        return LibertyTimesCrawler()
    elif news_source == 'http://tw.entertainment.appledaily.com/':
        return AppleDailyEntertainmentCrawler()
    elif news_source == 'http://tw.finance.appledaily.com/':
        return AppleDailyFinanceCrawler()
    elif news_source == 'http://tw.news.appledaily.com/':
        return AppleDailyNewsCrawler()
    elif news_source == 'http://tw.sports.appledaily.com/':
        return AppleDailySportsCrawler()
    elif news_source == 'http://www.chinatimes.com/':
        return ChinaTimesCrawler()
    elif news_source == 'https://news.tvbs.com.tw/':
        return TBVSNewsCrawler()
    elif news_source == 'https://tw.appledaily.com/':
        return AppleDailyCrawler()
    elif news_source == 'https://tw.lifestyle.appledaily.com/':
        return AppleDailyLifestyleCrawler()
    elif news_source == 'https://udn.com/':
        return UDNCrawler()
    else:
        return None