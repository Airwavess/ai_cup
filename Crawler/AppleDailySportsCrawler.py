from NewsCrawler import NewsCrawler

class AppleDailySportsCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('AppleDailySports title: ',)
        title = ''
        for t in html.find(class_='ndArticle_leftColumn').find_all(['h1', 'h2']):
            title += t.text
        return title

    def get_article(self, html):
        print('AppleDailySports article: ',)
        article = ''
        for paragraph in html.find('article', class_='ndArticle_content').find_all('p'):
            article += paragraph.text
        return article