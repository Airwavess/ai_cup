import re

from bs4 import BeautifulSoup
import pandas as pd
import requests

import Crawler

def get_news_source(url):
    pattern = re.compile('https?://[\w.]+/')
    news_source = pattern.match(url).group()
    return news_source

"""
{'http://home.appledaily.com.tw/',
 'http://news.ltn.com.tw/',
 'http://tw.entertainment.appledaily.com/',
 'http://tw.finance.appledaily.com/',
 'http://tw.news.appledaily.com/',
 'http://tw.sports.appledaily.com/',
 'http://www.chinatimes.com/',
 'https://news.tvbs.com.tw/',
 'https://tw.appledaily.com/',
 'https://tw.lifestyle.appledaily.com/',
 'https://udn.com/'}
"""

if __name__ == "__main__":
    urls = ['http://home.appledaily.com.tw/article/index/20140821/36036093/news/',
            'http://news.ltn.com.tw/news/world/paper/909874',
            'http://tw.entertainment.appledaily.com/daily/20130924/35314584/',
            'http://tw.finance.appledaily.com/daily/20140824/36041683/',
            'http://tw.news.appledaily.com/headline/daily/20140920/36097225/',
            'http://tw.sports.appledaily.com/daily/20110623/33479530/',
            'http://www.chinatimes.com/newspapers/20150108001507-260107',
            'https://news.tvbs.com.tw/local/320390',
            'https://tw.appledaily.com/forum/daily/20170419/37622289/',
            'https://tw.lifestyle.appledaily.com/daily/20140503/35806497/',
            'https://udn.com/news/story/6928/3421141']

    for url in urls:
        news_source = get_news_source(url)
        crawler = Crawler.get_crawler(news_source)
        if crawler is not None:
            news_content = requests.get(url).text
            content = BeautifulSoup(news_content, 'html.parser')
            
            news_article_title = crawler.get_title(content)
            print(news_article_title)
            news_article = crawler.get_article(content)
