from NewsCrawler import NewsCrawler

class AppleDailyFinanceCrawler(NewsCrawler):
    def __init__(self):
        pass

    def get_title(self, html):
        print('AppleDailyFinance title: ',)
        return html.find('article').find('h2').text

    def get_article(self, html):
        print('AppleDailyFinance article: ',)
        article = ''
        for paragraph in html.find(class_='ndArticle_margin').find_all('p')[:-1]:
            article += paragraph.text
        return article